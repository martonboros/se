module Ops exposing (..)

import Char
import Flex exposing (Direction)
import DictList exposing (DictList)
import List.Extra exposing ((!!), zip, last)
import Maybe.Extra exposing ((?), isJust, isNothing)
import List exposing (length, reverse, drop, unzip)
import Lang exposing (..)
import Model exposing (..)
import SeRender exposing (snake, unsnake)
import Focus exposing (..)
import Navigation exposing (..)


update_exps_syntax : (Syntax -> Maybe Syntax) -> Locus -> Int -> List Exp -> Maybe (List Exp)
update_exps_syntax f loc n exps =
    (exps !! n)
        |> Maybe.andThen
            (\exp ->
                update_exp_syntax f loc exp
                    |> Maybe.andThen (\exp -> exps |> List.Extra.setAt n exp)
            )


update_exp_syntax : (Syntax -> Maybe Syntax) -> Locus -> Exp -> Maybe Exp
update_exp_syntax f loc_ exp_ =
    case ( exp_, loc_ ) of
        ( Apply dir fun arg, FApplyFun :: loc ) ->
            update_exp_syntax f loc fun
                |> Maybe.map (\new -> Apply dir new arg)

        ( Apply dir fun arg, FApplyArg :: loc ) ->
            update_exp_syntax f loc arg
                |> Maybe.map (\new -> Apply dir fun new)

        ( Case cases, (FCase n) :: FCasePattern :: loc ) ->
            let
                ( ps, exps ) =
                    cases |> unzip
            in
                (update_exps_syntax f loc n ps)
                    |> Maybe.map (\ps -> Case (zip ps exps))

        ( Case cases, (FCase n) :: FCaseResult :: loc ) ->
            let
                ( ps, exps ) =
                    cases |> unzip
            in
                (update_exps_syntax f loc n exps)
                    |> Maybe.map (\exps -> Case (zip ps exps))

        ( Tup exps, (FTup n) :: loc ) ->
            update_exps_syntax f loc n exps |> Maybe.map Tup

        ( exp, FPoint :: loc ) ->
            case f (SExp exp) of
                Just (SExp new_exp) ->
                    Just new_exp

                _ ->
                    Nothing

        _ ->
            Nothing


update_bindings_syntax : (Syntax -> Maybe Syntax) -> Locus -> Bindings -> Maybe Bindings
update_bindings_syntax f loc_ bindings =
    case loc_ of
        (FBinding n) :: FBindingValue :: loc ->
                    Just
                        (bindings
                            |> DictList.update n
                                (\m_binding ->
                                    case m_binding of
                                        Just ( m_t, exp ) ->
                                            Just
                                                ((update_exp_syntax f loc exp
                                                    |> Maybe.map (\exp -> ( m_t, exp ))
                                                 )
                                                    ? ( m_t, exp )
                                                )

                                        _ ->
                                            Nothing
                                )
                        )

        [ FBinding n, FBindingName, FPoint ] ->
            case bindings |> DictList.get n of
                Just value ->
                    case f (SName n) of
                        Just (SName new_n) ->
                            if n == new_n then
                                Nothing
                            else
                                bindings |> DictList.insertAfter n new_n value |> DictList.remove n |> Just

                        _ ->
                            Nothing

                _ ->
                    Nothing

        --
        --(FBinding n) :: FBindingTyp :: loc ->
        --    bindings
        --        |> DictList.getAt n
        --        |> Maybe.andThen
        --            ((Tuple.second >> Tuple.first)
        --                >> (Maybe.map (\typ -> walk_typ typ loc))
        --            )
        --        |> Maybe.Extra.join
        --(FBinding n) :: FPoint :: loc ->
        --    bindings
        --        |> DictList.getAt n
        --        |> Maybe.map
        --            (\( n, ( m_t, v ) ) -> SBinding n m_t v)
        [ FPoint ] ->
            case f (SBindings bindings) of
                Just (SBindings new_bindings) ->
                    Just new_bindings

                _ ->
                    Nothing

        _ ->
            Nothing



--update_exp_dir : Direction -> Exp -> Locus -> Maybe Exp
--update_exp_dir dir_ exp loc =
--    let
--        f exp =
--            case exp of
--                Apply dir fun arg ->
--                    Just (Apply dir_ fun arg)
--                _ ->
--                    Nothing
--    in
--        update_exp f loc exp


update_bindings_dir : Direction -> Bindings -> List Focus -> Maybe Bindings
update_bindings_dir dir_ bindings loc =
    let
        f syn =
            case syn of
                SExp (Apply dir fun arg) ->
                    Just (SExp (Apply dir_ fun arg))

                _ ->
                    Nothing
    in
        update_bindings_syntax f loc bindings


replace_globals : String -> String -> DictList String () -> Exp -> Exp
replace_globals old new locals exp =
    case exp of
        Apply dir fun arg ->
            Apply dir (replace_globals old new locals fun) (replace_globals old new locals arg)

        Var ( Just q, name ) ->
            exp

        Var ( Nothing, name ) ->
            if name == old && (locals |> DictList.get name |> isNothing) then
                Var ( Nothing, new )
            else
                exp

        Tup exps ->
            exps
                |> List.map (replace_globals old new locals)
                |> Tup

        Case cases ->
            cases
                |> List.map (\( p, r ) -> ( p, replace_globals old new (DictList.union locals (get_pvars p)) r ))
                |> Case

        _ ->
            exp

validate_update_bindings_string : String -> Locus -> Bindings -> Bool
validate_update_bindings_string str loc bindings =
    case loc of
        [FBinding n, FBindingName, FPoint] ->
            not (bindings |> DictList.keys |> List.member str)
        _ ->
            True

update_bindings_string : String -> Locus -> Bindings -> Maybe Bindings
update_bindings_string str loc bindings =
    let
        f syn =
            case syn of
                SExp (Var ( m_q, name )) ->
                    Just (SExp (Var ( m_q, str )))

                SExp (Lit (String s)) ->
                    Just (SExp (Lit (String str)))

                SExp (Lit (Int _)) ->
                    case String.toInt str of
                        Ok x ->
                            Just (SExp (Lit (Int x)))

                        _ ->
                            Nothing

                SExp (Lit (Float _)) ->
                    case String.toFloat str of
                        Ok x ->
                            Just (SExp (Lit (Float x)))

                        _ ->
                            Nothing

                SName n ->
                    Just (SName str)

                _ ->
                    Nothing
    in
        if not <| validate_update_bindings_string str loc bindings then  Nothing else
        case  ( update_bindings_syntax f loc bindings, walk_bindings bindings loc ) of
            ( Just bindings, Just (SName name) ) ->
                Just
                    (bindings
                        |> DictList.toList
                        |> List.map
                            (\( n, ( m_t, v ) ) ->
                                ( n, ( m_t, v |> replace_globals name str DictList.empty ) )
                            )
                        |> DictList.fromList
                    )

            ( Just bindings, Just (SExp (Var (m_q, name)) )) ->
                if (loc |> List.filter (\f -> f == FCasePattern) |> List.length ) == 1 then

                    let
                        result_loc = (loc |> List.Extra.takeWhile (( /= ) FCasePattern)) ++ [FCaseResult, FPoint]
                        f syn =
                            case  syn of
                                SExp exp ->
                                    Just (SExp (replace_var name str exp))
                                _ ->
                                    Nothing
                    in
                         update_bindings_syntax f result_loc bindings

                else
                    Just bindings

            ( Just bindings, _ ) ->
                Just bindings

            _ ->
                Nothing

update_locus_string : String -> Locus  -> Locus -> Locus
update_locus_string str cursor loc =
    case (cursor, loc) of
        (FBinding n :: FBindingName :: FPoint :: [], FBinding m ::  fs) ->
            if m == n then
                FBinding str :: fs
            else
                loc
        _ ->
            loc


update_model_string : String -> Model -> Model
update_model_string input model =
            { model
                | cursor = update_locus_string input model.cursor model.cursor
                , pointer = update_locus_string input model.cursor model.pointer
                , modul =
                    case update_bindings_string input model.cursor model.modul.bindings of
                        Just bindings ->

                                model.modul |> (\m -> { m | bindings = bindings })
                        _ -> model.modul
            }



replace_var : String -> String -> Exp -> Exp
replace_var from to exp =
    case exp of
        Apply direction exp exp2 ->
            Apply direction (replace_var from to exp) (replace_var from to exp2)

        Var ( Nothing, name ) ->
            if name == from then
                Var ( Nothing, to )
            else
                exp

        Tup exps ->
            Tup (exps |> List.map (replace_var from to))

        Case cases ->
            Case
                (cases
                    |> List.map
                        (\( p, r ) ->
                            ( p
                            , if get_pvars p |> DictList.get from |> isJust then
                                r
                              else
                                (replace_var from to r)
                            )
                        )
                )

        _ ->
            exp



new_name : DictList String a -> String
new_name bindings =
 let     try ns bs =
        case ns of
            (n :: m :: ns) ->
                case bs |> DictList.get n of
                    Just _ ->
                        try (m :: ns) bs
                    Nothing ->
                        n
            n :: [] ->
                case  bs |> DictList.get n of
                    Just _ ->
                        try ["unnamed" ++ ((((n |>
                             String.filter Char.isDigit |> String.toInt |> Result.toMaybe ) ? 0) + 1) |> toString)] bs
                    Nothing ->
                        n

            [] ->
                "_|_"
    in
        try [ "f1"] bindings



new_binding : Model -> Model
new_binding model =
        let
            modul =
                model.modul
            name = new_name model.modul.bindings
        in
            { model
                | modul = { modul | bindings = modul.bindings |> DictList.insert name
                    ( Just (TTup []), Var (Nothing, "?" )) }
                , cursor = [ Focus.FBinding name, Focus.FBindingName, Focus.FPoint ]
                , input = Just name
            }

accept_input : String ->Model -> Model
accept_input i model =
    let
        modul =
            model.modul

        unsnaked =
            (if model.opts.snake then
                unsnake i
             else
                i
            )
        new_model = update_model_string unsnaked model
    in
        { new_model | input = Nothing }


start_input :  Model -> Model
start_input model =
    case Navigation.walk_bindings model.modul.bindings model.cursor of
        Just (SExp (Var ( _, n ))) ->
            { model
                | input =
                    Just
                        (if model.opts.snake then
                            snake n
                         else
                            n
                        )
            }

        Just (SExp (Lit l)) ->
            { model | input = Just (litToString l) }

        Just (SName n) ->
            { model
                | input =
                    Just
                        (if model.opts.snake then
                            snake n
                         else
                            n
                        )
            }

        _ ->
            model
