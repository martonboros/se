module Model exposing (..)

import Keyboard.Extra
import Flex exposing (Direction)
import Lang exposing (..)
import Focus exposing (..)


type Msg
    = Nop
    | OptionsMsg OptionsMsg
    | ChangeSrc String
    | ChangePointer (List Focus)
    | ChangeCursor (List Focus)
    | ChangeDirection (List Focus) Direction
    | KeyDown Keyboard.Extra.Key
    | KeyMsg Keyboard.Extra.Msg
    | ChangeInput String


type OptionsMsg
    = Source Bool
    | AssociativeApply Bool
    | Borders Bool
    | Alternating Bool
    | Gradient Bool
    | CollapseVertical Bool
    | Parens Bool
    | Qualifiers Bool
    | Infix Bool
    | Snake Bool
    | Direction Bool
    | DebugKeysPressed Bool
    | DebugCursorPointer Bool
    | DebugInput Bool
    | ShowOptions Bool


type alias Model =
    { modul : Module
    , src : String
    , opts : Options
    , show_opts : Bool
    , keys_pressed : List Keyboard.Extra.Key
    , cursor : Locus
    , pointer : Locus
    , input : Maybe String
    , clips : List Syntax
    }


init_model : Model
init_model =
    { modul = init_module
    , src = ""
    , opts = init_opts
    , show_opts = False
    , keys_pressed = []
    , cursor = []
    , pointer = []
    , input = Nothing
    , clips = []
    }


type alias Options =
    { source : Bool
    , associative_apply : Bool
    , borders : Bool
    , alternating : Bool
    , gradient : Bool
    , collapse_vertical : Bool
    , parens : Bool
    , qualifiers : Bool
    , infix : Bool
    , snake : Bool
    , direction : Bool
    , debugKeysPressed : Bool
    , debugCursorPointer : Bool
    , debugInput : Bool
    , showOptions : Bool
    }


init_opts : Options
init_opts =
    { source = False
    , associative_apply = True
    , borders = False
    , alternating = True
    , gradient = False
    , collapse_vertical = True
    , parens = False
    , qualifiers = False
    , infix = True
    , snake = True
    , direction = False
    , debugKeysPressed = False
    , debugCursorPointer = False
    , debugInput = False
    , showOptions = False
    }
