module SeRun exposing (..)


type alias I_ a =
    a


i_ : a -> a
i_ =
    always
