'use strict';

require('./index.html');
var Elm = require('./Main');

var elm = Elm.Main.fullscreen({
  swapCount: 0
});

// elm.ports.code.subscribe(function(message) {
// })

// interop
elm.ports.select.subscribe(function([a, b]) {
    setTimeout(() => {
        document.getElementById("input").focus()
        document.getElementById("input").setSelectionRange(a, b);
    }, 0)
});

//elm-hot callback
elm.hot.subscribe(function (event, context) {
  console.log('elm-hot event:', event)
  context.state.swapCount ++
})
