module SeStyle exposing (..)

import Basics.Extra exposing ((=>))


type alias Style =
    List ( String, String )


base00 : String
base00 =
    "#263238"


base01 : String
base01 =
    "#2C393F"


base02 : String
base02 =
    "#37474F"


base03 : String
base03 =
    "#707880"


base04 : String
base04 =
    "#C9CCD3"


base05 : String
base05 =
    "#CDD3DE"


base06 : String
base06 =
    "#dddddd"



--"#D5DBE5"


base07 : String
base07 =
    "#FFFFFF"


base08 : String
base08 =
    "#EC5F67"


base09 : String
base09 =
    "#EA9560"


base0A : String
base0A =
    "#FFCC00"


base0B : String
base0B =
    "#8BD649"


base0C : String
base0C =
    "#80CBC4"


base0D : String
base0D =
    "#89DDFF"


base0E : String
base0E =
    "#82AAFF"


base0F : String
base0F =
    "#EC5F67"


darkgrey : String
darkgrey =
    "#222222"


grey : String
grey =
    "#DADADA"


brightgrey : String
brightgrey =
    "#AAAAAA"


orange : String
orange =
    "#E8A31B"


salmon : String
salmon =
    "#f95725"


yellow : String
yellow =
    "#eadc62"


green : String
green =
    "#00c177"


cyan : String
cyan =
    "#00c1b0"


purple : String
purple =
    "#4c2030"


bg : String -> Style
bg color =
    [ "background-color" => color ]


fg : String -> Style
fg color =
    [ "color" => color ]


bc : String -> Style
bc color =
    [ "border-color" => color ]


bw : String -> Style
bw width =
    [ "border-width" => width ]


brd : String -> Style
brd b =
    [ "border" => b ]


br : String -> Style
br r =
    [ "border-radius" => r ]


c : String -> Style
c color =
    fg color ++ bc color
