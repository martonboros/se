module SeRender exposing (..)

import Char
import Html exposing (Html, div, span, label, text)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput, onMouseEnter, onMouseLeave, onWithOptions)
import Basics.Extra exposing ((=>))
import Json.Decode
import Maybe.Extra exposing ((?))
import Flex exposing (..)
import SeStyle exposing (..)
import SeFlex exposing (..)
import Lang
import Focus exposing (..)
import ViewContext exposing (..)
import Model exposing (..)


view_source : String -> Html Msg
view_source src =
    Html.textarea
        [ cols 80
        , rows 12
        , style (bg "black" ++ fg "white")
        , onInput Model.ChangeSrc
        ]
        [ text src ]


google_fonts : String
google_fonts =
    "https://fonts.googleapis.com/css?family="
        ++ "Fira+Sans:200,200i&amp;subset=greek,greek-ext,latin-ext"


css_ : String
css_ =
    """
html {
    background-color: #000;
    color: """ ++ base07 ++ """;
    font-family: Fira Sans, sans-serif;
    font-size: 20px;

}
*{
    user-select:none;
    outline : none;
    box-sizing: border-box;
    padding: 0px;
    margin: 0px;
}
"""


checkbox : String -> (Bool -> msg) -> Bool -> Html msg
checkbox name msg val =
    label
        [ style [ ( "padding", "10px" ) ] ]
        [ Html.input [ type_ "checkbox", onClick <| msg (not val), checked val ] []
        , text name
        ]


snake : String -> String
snake str =
    if Lang.is_capital str then
        str
    else
        str
            |> String.toList
            |> List.indexedMap
                (\index char ->
                    if Char.isUpper char && index > 0 then
                        [ '-', Char.toLower char ]
                    else
                        [ char ]
                )
            |> List.concat
            |> String.fromList


unsnake : String -> String
unsnake str =
    str
        |> String.toList
        |> List.foldl
            (\char ( acc, was_hyphen ) ->
                ( if char == '-' then
                    acc
                  else if was_hyphen then
                    (Char.toUpper char) :: acc
                  else
                    char :: acc
                , char == '-'
                )
            )
            ( [], False )
        |> Tuple.first
        |> List.reverse
        |> String.fromList


opt_snake : Options -> String -> String
opt_snake opts str =
    if opts.snake then
        snake str |> String.split "-" |> String.join "-\xFEFF"
    else
        str


ligature : String -> String
ligature name =
    case name of
        "|>" ->
            "▷"

        "<|" ->
            "◁"

        _ ->
            name


keyword : String -> Html a
keyword str =
    ct (fg "rgba(255,255,255,0.4)") [ text (" " ++ str ++ " ") ]


paren : String -> Options -> Html msg
paren str opts =
    ct (c base03)
        [ text
            (if opts.parens then
                str
             else
                ""
             --"\x205F"
            )
        ]


lparen : Options -> Html msg
lparen =
    paren "("


rparen : Options -> Html msg
rparen =
    paren ")"


dir_paren : Direction -> (Options -> Html msg)
dir_paren dir =
    if is_reverse dir then
        rparen
    else
        lparen


direction : String -> Options -> Html msg
direction str opts =
    ct (c base03) [ text str ]


ldirection : Options -> Html msg
ldirection opts =
    ct (c base03)
        [ text
            (if opts.direction then
                "◁"
             else
                "\x205F"
            )
        ]


rdirection : Options -> Html msg
rdirection opts =
    ct (c base03) [ text "▶" ]


dir_direction : Direction -> Options -> Html msg
dir_direction dir =
    case dir of
        Horizontal ->
            ldirection

        HorizontalReverse ->
            rdirection

        Vertical ->
            \_ -> ct (c base03 ++ [ "font-size" => "0.7em" ]) [ text "▲" ]

        VerticalReverse ->
            \_ -> ct (c base03 ++ [ "font-size" => "0.7em" ]) [ text "▼" ]


box_color : Context -> String
box_color ctx =
    (\n ->
        "hsl(20, "
            ++ toString
                ((if False && ctx.cursor /= [] then
                    3 - (List.length ctx.cursor)
                  else
                    0
                 )
                    * 40
                )
            ++ "%, "
            ++ toString
                (ctx.depth
                    * (if ctx.opts.gradient then
                        2
                       else
                        0
                      )
                    + n
                    + ((if False && List.length ctx.cursor == 2 then
                            3 - (List.length ctx.cursor)
                        else
                            0
                       )
                        * 10
                      )
                )
            ++ "%)"
    )
    <|
        (if ctx.depth % 2 == 1 then
            (if ctx.opts.alternating then
                12
             else
                7
            )
         else
            7
        )
            - (if ctx.opts.gradient && ctx.depth > 20 then
                (ctx.depth - 20) * 2
               else
                0
              )



--if rem ctx.depth 2 == 0 then
--    "#111"
--else
--    "#222"


border_color : Context -> String
border_color ctx =
    (if ctx.depth % 2 == 1 then
        (if ctx.opts.alternating then
            "#444"
         else
            "#555"
        )
     else
        "#555"
    )



-- box_color ctx


vertical_border : Direction -> Context -> Style
vertical_border dir ctx =
    (if not ctx.opts.borders then
        [ "border-style" => "none" ]
     else
        []
    )
        ++ (bg (box_color ctx))
        --++ [ "padding" => "4px" ]
        ++
            (br "3px")
        ++ (if ctx.opts.parens || is_vertical dir then
                []
            else
                (if ctx.last_vertical_top then
                    []
                    --[ "border-bottom-color" => border_color ctx ]
                 else
                    []
                 --[ "border-top-color" => border_color ctx ]
                )
           )
        ++ (if ctx.selected then
                bg purple
            else
                []
           )


with_border1 : Direction -> Context -> Style
with_border1 dir ctx =
    if ctx.opts.parens then
        []
    else
        bc (border_color ctx)
            ++ (if ctx.opts.borders then
                    [ "border-radius" => "0px" ]
                else
                    []
               )
            ++ [ "padding-top" => "4px", "padding-bottom" => "4px", "padding-left" => "4px", "padding-right" => "4px" ]
            --++ [ "padding" => "4px" ]
            ++
                (if ctx.opts.collapse_vertical then
                    (if (not (is_vertical dir)) then
                        [ "padding-left" => "4px", "padding-right" => "4px" ]
                            ++ (if not ctx.last_vertical_top then
                                    [ "padding-bottom" => "0px"
                                    , "border-bottom-width"
                                        => if ctx.opts.borders || ctx.cursor == [ FPoint ] then
                                            "1px"
                                           else
                                            --"1px"
                                            "0"
                                    ]
                                else
                                    [ "padding-top" => "0px"
                                    , "border-top-width"
                                        => if ctx.opts.borders || ctx.cursor == [ FPoint ] then
                                            "1px"
                                           else
                                            --"1px"
                                            "0"
                                    ]
                               )
                     else
                        []
                    )
                        ++ if (ctx.m_dir |> Maybe.map is_vertical) ? False then
                            --[ "border-left-color" => "rgba(0,0,0,0)", "border-right-color" => "rgba(0,0,0,0)", "padding-left" => "0px", "padding-right" => "0px" ]
                            --++
                            if ctx.last_vertical_top && not (is_vertical dir) then
                                [ "border-top-color" => "rgba(0,0,0,0)" ]
                            else
                                [ "border-bottom-width" => "rgba(0,0,0,0)" ]
                           else
                            []
                 else
                    []
                )
            ++ (if ctx.selected then
                    bg purple
                else
                    []
               )


default_key_script : String
default_key_script =
    """window.addEventListener("keydown", (e) => { if (e.key.indexOf("Arrow") != -1 ) { e.preventDefault() }})"""


pointer_msgs : Context -> List (Html.Attribute Model.Msg)
pointer_msgs ctx =
    [ onMouseEnter
        (ChangePointer (List.reverse (FPoint :: ctx.here)))
    , onMouseLeave
        (ChangePointer (List.reverse (FPoint :: (List.tail ctx.here ? []))))
    , onWithOptions "click"
        (Html.Events.Options True True)
        (Json.Decode.succeed (Model.ChangeCursor (List.reverse (FPoint :: ctx.here))))
    ]


to_css : Style -> String
to_css style =
    style
        |> List.map (\( k, v ) -> k ++ ": " ++ v ++ ";\n")
        |> String.concat


input_style : Style
input_style =
    (border ++ bg purple ++ fg "white" ++ bc "white" ++ br "3px")
        ++ [ "font-family" => "Fira Sans, sans-serif"
           , "font-size" => "20px"
           , "width" => "100%"
           , "margin" => "1px"
           , "padding" => "0px"
           ]


border : Style
border =
    [ "border-style" => "solid" ] ++ bc "rgba(0,0,0,0)"



--[ "border-width" => "1px", "border-style" => "solid" ] ++


pointer_style : Context -> Style
pointer_style ctx =
    List.concat
        [ maybe_fpoint ctx.pointer (bc "#fff" ++ br "2px") ? []
        , maybe_fpoint ctx.cursor (bg purple ++ bc salmon ++ br "2px") ? []
        ]


cursor_style : Context -> Style
cursor_style ctx =
    maybe_fpoint ctx.cursor
        (bg purple ++ bc salmon ++ br "3px" ++ [ "border-width" => "1px", "border-style" => "solid" ])
        ? (bc "transparent" ++ [ "border-width" => "1px", "border-style" => "solid" ])


binding_style : Style
binding_style =
    bg "#111" ++ br "3px"


binding_name_style : Style
binding_name_style =
    --c base0E
    fg yellow


binding_header_style : Style
binding_header_style =
    br "3px 3px 0 0" ++ bg "#222" ++ [ "width" => "100%", "padding" => "6px 8px 4px 8px" ]


binding_body_style : Style
binding_body_style =
    [ "width" => "100%", "padding" => "4px 8px 8px   8px" ]


table_style : Style
table_style =
    [ "border-spacing" => "0px" ]


arg_name_style : Style
arg_name_style =
    fg green


tcons_name_style : Style
tcons_name_style =
    --c base0F
    --c base0B
    fg orange


global_name_style : Style
global_name_style =
    fg grey


own_name_style : Style
own_name_style =
    fg yellow


qualifier_style : Style
qualifier_style =
    fg base03


case_style : Style
case_style =
    -- bc base0E ++ fg grey
    --bc base02 ++
    [ "padding" => "4px" ]


tvar_style : Style
tvar_style =
    var_style ++ fg cyan


tarrow_style : Style
tarrow_style =
    fg grey


var_style : Style
var_style =
    fg cyan


lit_style : Style
lit_style =
    border ++ fg "#aaa" ++ bg "rgba(255,255,255,0.1 )" ++ br "3px" ++ [ "padding" => "0px", "margin" => "0px" ]
