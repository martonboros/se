module Package exposing (..)


type alias Version = String

type alias VersionConstraint = String

type alias RepositoryURL

type alias Package =
    { version : Version
    , summary: String
    , repository : RepositoryURL
    , license : String
    , sourceDirectories : List String
    , exposedModules : List String
    , depedencies : DictList String VersionConstraint
    , elmVersion : VersionConstraint
    }
