module ViewContext exposing (..)

import DictList exposing (DictList)
import Flex exposing (Direction(..))
import Maybe.Extra exposing ((?))
import Lang exposing (..)
import Model exposing (..)
import Focus exposing (..)
import SeFlex exposing (..)
import Navigation exposing (..)


type alias Context =
    { model : Model
    , opts : Options
    , this : Syntax
    , parent : Syntax
    , here : Locus
    , pointer : Locus
    , cursor : Locus
    , selected : Bool
    , is_pattern : Bool
    , m_dir : Maybe Direction
    , locals : DictList String ()
    , modul : Module
    , name : String
    , input : Maybe String
    , last_vertical_top : Bool
    , depth : Int
    }


make_context : Model -> Context
make_context model =
    Context
        model
        model.opts
        (SBindings model.modul.bindings)
        (SName "?")
        []
        model.pointer
        model.cursor
        False
        False
        Nothing
        -- (Just Vertical)
        DictList.empty
        model.modul
        ""
        model.input
        False
        0


is_deeper : Context -> Bool
is_deeper ctx =
    case ( ctx.parent, ctx.this ) of
        ( SCase _ _, SExp (Apply dir fun arg) ) ->
            True

        --True
        ( _, SExp (Apply dir fun arg) ) ->
            not ctx.opts.associative_apply
                || is_vertical dir
                || (ctx.m_dir == Just Vertical)
                || (ctx.m_dir == Just VerticalReverse)
                || (ctx.m_dir == Just Horizontal && is_arg ctx)
                || (ctx.m_dir == Just HorizontalReverse && is_arg ctx)

        ( _, SExp (Tup _) ) ->
            True

        --not ctx.opts.associative_apply
        ( _, SExp (Case _) ) ->
            True

        _ ->
            False



--|| (ctx.m_dir == Just HorizontalReverse && is_fun ctx)
--||
-- (dir == HorizontalReverse)


zoom : Focus -> Context -> Context
zoom focus ctx =
    let
        this =
            walk_syntax ctx.this [ focus ] ? SName "?"
    in
        { ctx
            | here = focus :: ctx.here
            , this = this
            , parent = ctx.this
            , pointer = zoom_point focus ctx.pointer
            , cursor = zoom_point focus ctx.cursor
            , selected = ctx.selected || zoom_point focus ctx.cursor == [ FPoint ]
        }
            |> (\ctx ->
                    { ctx
                        | depth =
                            if is_deeper ctx then
                                ctx.depth + 1
                            else
                                ctx.depth
                    }
               )


is_fun : Context -> Bool
is_fun ctx =
    (List.head ctx.here == Just FApplyFun)


is_arg : Context -> Bool
is_arg ctx =
    List.head ctx.here == Just FApplyArg


deeper : Context -> Context
deeper ctx =
    { ctx | depth = ctx.depth + 1 }
