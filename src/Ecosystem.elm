module Ecosystem exposing (..)


type alias Ecosystem =
    List Package


type alias Package =
    { name : String
    , description : String
    , modules : List String
    }
