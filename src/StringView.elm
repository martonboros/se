module StringView exposing (..)

import Maybe.Extra exposing ((?))
import DictList
import String exposing (join)
import List exposing (map)
import Lang exposing (..)


str_typ : Typ -> String
str_typ typ =
    case typ of
        TVar name ->
            name

        TCons ( m_q, n ) ts ->
            ((m_q
                |> Maybe.map
                    (\q ->
                        q
                            ++ (if q /= "" then
                                    "."
                                else
                                    ""
                               )
                            ++ n
                    )
             )
                ? n
            )
                ++ " "
                ++ (ts
                        |> map str_typ
                        |> join " "
                   )

        TArrow t1 t2 ->
            str_typ t1 ++ " -> " ++ str_typ t2

        TRecord r ->
            toString r

        TTup typs ->
            "(" ++ (typs |> List.map str_typ |> String.join ", ") ++ ")"


str_exp : Int -> Exp -> String
str_exp level exp_ =
    case exp_ of
        Apply _ (Apply _ (Var ( ns, f )) arg1) arg2 ->
            if is_infix f then
                "(" ++ str_exp level arg1 ++ " " ++ str_exp level (Var ( ns, f )) ++ " " ++ str_exp level arg2 ++ ")"
            else
                "((" ++ str_exp level (Var ( ns, f )) ++ " " ++ str_exp level arg1 ++ ") " ++ str_exp level arg2 ++ ")"

        Apply dir fun arg ->
            "(" ++ str_exp level fun ++ " " ++ str_exp level arg ++ ")"

        --Let bindings exp ->
        --    indent level ++ "let" ++ (bindings |> str_bindings (level + 1)) ++ "\n" ++ indent level ++ "in\n" ++ str_exp (level + 1) exp
        --Lam arg exp ->
        --    "(\\" ++ arg ++ " ->\n" ++ indent (level + 1) ++ str_exp (level + 1) exp ++ ")"
        Var name ->
            Tuple.second name

        Lit lit ->
            str_literal lit

        Tup exps ->
            "( " ++ (exps |> List.map (str_exp level) |> String.join ", ") ++ " )"

        Case (( Var ( Nothing, name ), exp ) :: []) ->
            "\\" ++ name ++ " -> " ++ str_exp level exp

        Case cases ->
            "(\\e -> case e of\n"
                ++ (cases
                        |> List.map
                            (\( p, res ) ->
                                indent (level + 1)
                                    ++ str_exp (level + 1) p
                                    ++ " -> \n"
                                    ++ indent (level + 2)
                                    ++ str_exp (level + 2) res
                            )
                        |> String.join "\n"
                   )
                ++ "\n"
                ++ indent (level + 1)
                ++ ")"

        Record bindings ->
            "{" ++ str_bindings (level + 1) (bindings |> DictList.map (\k v -> ( Nothing, v ))) ++ indent level ++ "}"


str_literal : Literal -> String
str_literal lit =
    case lit of
        String x ->
            "\"" ++ x ++ "\""

        Char x ->
            "'" ++ toString x ++ "'"

        Int x ->
            toString x

        Float x ->
            toString x


str_binding : Int -> ( String, ( Maybe Typ, Exp ) ) -> String
str_binding level ( name, ( m_typ, exp ) ) =
    let
        type_annotation =
            case m_typ of
                Just typ ->
                    indent level ++ name ++ " : " ++ str_typ typ ++ "\n"

                Nothing ->
                    ""
    in
        type_annotation
            ++ indent level
            ++ name
            ++ " = \n"
            ++ indent (level + 1)
            ++ str_exp (level + 1) exp


str_bindings : Int -> Bindings -> String
str_bindings level bs =
    bs
        |> DictList.toList
        |> List.map (str_binding level)
        |> String.join "\n"
        |> (++) "\n"


indent : Int -> String
indent level =
    List.repeat level "    " |> String.join ""
