module Navigation exposing (..)

import Flex exposing (Direction(..))
import Keyboard.Extra exposing (Key(..))
import DictList
import List exposing (length, reverse, drop, unzip)
import List.Extra exposing ((!!), zip, last)
import Lang exposing (..)
import Model exposing (..)
import Focus exposing (..)


walk_syntax : Syntax -> Locus -> Maybe Syntax
walk_syntax syn locus =
    case syn of
        SBindings bindings ->
            walk_bindings bindings locus

        SBinding name m_typ exp ->
            walk_binding ( name, ( m_typ, exp ) ) locus

        SCase pat res ->
            walk_case ( pat, res ) locus

        STyp typ ->
            walk_typ typ locus

        SExp exp ->
            walk_exp exp locus

        _ ->
            Nothing


walk_binding : ( String, ( Maybe Typ, Exp ) ) -> Locus -> Maybe Syntax
walk_binding ( n, ( m_t, v ) ) locus =
    case locus of
        FBindingName :: [] ->
            Just (SName n)

        FBindingName :: [ FPoint ] ->
            Just (SName n)

        FBindingValue :: loc ->
            walk_exp v loc

        FBindingTyp :: loc ->
            m_t |> Maybe.andThen (\typ -> walk_typ typ loc)

        [] ->
            Just (SBinding n m_t v)

        [ FPoint ] ->
            Just (SBinding n m_t v)

        _ ->
            Nothing


walk_bindings : Bindings -> Locus -> Maybe Syntax
walk_bindings bindings locus =
    case locus of
        (FBinding n) :: loc ->
            bindings
                |> DictList.get n
                |> Maybe.andThen (\b -> walk_binding ( n, b ) loc)

        [ FPoint ] ->
            Just (SBindings bindings)

        [] ->
            Just (SBindings bindings)

        _ ->
            Nothing


walk_typ : Typ -> Locus -> Maybe Syntax
walk_typ typ loc =
    case ( typ, loc ) of
        ( TCons qname typs, FTConsName :: locs ) ->
            Just (SQualifiedName qname)

        ( TCons qname typs, (FTConsArg n) :: locs ) ->
            (typs !! n) |> Maybe.andThen (\t -> walk_typ t locs)

        ( TArrow typ1 typ2, FTArrowArg :: locs ) ->
            walk_typ typ1 locs

        ( TArrow typ1 typ2, FTArrowResult :: locs ) ->
            walk_typ typ2 locs

        ( TTup typs, (FTTup n) :: locs ) ->
            (typs !! n) |> Maybe.andThen (\t -> walk_typ t locs)

        ( _, [ FPoint ] ) ->
            Just (STyp typ)

        ( _, [] ) ->
            Just (STyp typ)

        _ ->
            Nothing


walk_exp : Exp -> Locus -> Maybe Syntax
walk_exp exp_ at_ =
    case ( exp_, at_ ) of
        ( Apply dir fun arg, FApplyFun :: loc ) ->
            walk_exp fun loc

        ( Apply dir fun arg, FApplyArg :: loc ) ->
            walk_exp arg loc

        ( Case cases, (FCase n) :: loc ) ->
            (cases !! n) |> Maybe.andThen (\cas -> walk_case cas loc)

        ( Tup exps, (FTup n) :: loc ) ->
            (exps !! n)
                |> Maybe.andThen (\exp -> walk_exp exp loc)

        ( exp, FPoint :: loc ) ->
            Just (SExp exp)

        ( exp, [] ) ->
            Just (SExp exp)

        _ ->
            Nothing


walk_case : ( Exp, Exp ) -> Locus -> Maybe Syntax
walk_case ( pat, res ) locus =
    case ( 0, locus ) of
        ( 0, FCasePattern :: loc ) ->
            walk_exp pat loc

        ( 0, FCaseResult :: loc ) ->
            walk_exp res loc

        ( 0, [ FPoint ] ) ->
            Just (SCase pat res)

        ( 0, [] ) ->
            Just (SCase pat res)

        _ ->
            Nothing


descend_right : Syntax -> Locus -> Locus
descend_right syn acc =
    case syn of
        SExp exp ->
            descend_right_exp exp acc

        SCase p r ->
            descend_right_exp r [ FCaseResult ]

        STyp t ->
            descend_right_typ t acc

        _ ->
            reverse acc


descend_left : Syntax -> Locus -> Locus
descend_left syn acc =
    case syn of
        SExp exp ->
            descend_left_exp exp acc

        SCase p r ->
            descend_left_exp p [ FCasePattern ]

        STyp t ->
            descend_left_typ t acc

        _ ->
            reverse acc


descend_right_exp : Exp -> Locus -> Locus
descend_right_exp exp_ acc =
    case exp_ of
        Apply Horizontal fun arg ->
            descend_right_exp arg (FApplyArg :: acc)

        Apply HorizontalReverse fun arg ->
            descend_right_exp fun (FApplyFun :: acc)

        Apply Vertical fun arg ->
            descend_right_exp fun (FApplyFun :: acc)

        Apply VerticalReverse fun arg ->
            descend_right_exp arg (FApplyArg :: acc)

        Tup exps ->
            case reverse exps of
                exp :: _ ->
                    descend_left_exp exp (FTup (length exps - 1) :: acc)

                [] ->
                    reverse acc

        Case cases ->
            case drop (length cases - 1) cases of
                ( p, exp ) :: _ ->
                    descend_right_exp exp (FCase (length cases - 1) :: FCaseResult :: acc)

                _ ->
                    reverse acc

        _ ->
            reverse acc


descend_right_typ : Typ -> Locus -> Locus
descend_right_typ typ_ acc =
    case typ_ of
        TCons qname args ->
            case List.Extra.last args of
                Just arg ->
                    descend_right_typ arg (FTConsArg (List.length args - 1) :: acc)

                Nothing ->
                    List.reverse (FTConsName :: acc)

        TArrow arg result ->
            descend_right_typ result (FTArrowResult :: acc)

        TRecord bindings ->
            Debug.crash "TODO"

        TTup typs ->
            case last typs of
                Just typ ->
                    descend_right_typ typ (FTTup (length typs - 1) :: acc)

                Nothing ->
                    List.reverse acc

        TVar name ->
            List.reverse acc


descend_left_exp : Exp -> Locus -> Locus
descend_left_exp exp_ acc =
    case exp_ of
        Apply Horizontal fun arg ->
            descend_left_exp fun (FApplyFun :: acc)

        Apply HorizontalReverse fun arg ->
            descend_left_exp arg (FApplyArg :: acc)

        Apply Vertical fun arg ->
            descend_left_exp fun (FApplyFun :: acc)

        Apply VerticalReverse fun arg ->
            descend_left_exp arg (FApplyArg :: acc)

        Tup exps ->
            case exps of
                exp :: _ ->
                    descend_left_exp exp (FTup 0 :: acc)

                [] ->
                    List.reverse acc

        Case (( p, exp ) :: _) ->
            descend_left_exp p (FCasePattern :: FCase 0 :: acc)

        _ ->
            List.reverse acc


descend_left_typ : Typ -> Locus -> Locus
descend_left_typ typ_ acc =
    case typ_ of
        TCons qname args ->
            List.reverse (FTConsName :: acc)

        TArrow arg result ->
            descend_left_typ result (FTArrowArg :: acc)

        TRecord bindings ->
            Debug.crash "TODO"

        TTup typs ->
            case last typs of
                Just typ ->
                    descend_left_typ typ (FTTup 0 :: acc)

                Nothing ->
                    List.reverse acc

        TVar name ->
            List.reverse acc


ascend_left : Bindings -> Locus -> Locus
ascend_left bs loc =
    let
        def _ =
            ascend_left bs (remove_last loc)
    in
        case get_last loc of
            Just focus ->
                case focus of
                    FApplyFun ->
                        case walk_bindings bs (remove_last loc) of
                            Just (SExp (Apply HorizontalReverse fun exp)) ->
                                update_last FApplyArg loc

                            _ ->
                                def ()

                    FApplyArg ->
                        case walk_bindings bs (remove_last loc) of
                            Just (SExp (Apply Horizontal fun exp)) ->
                                update_last FApplyFun loc

                            _ ->
                                def ()

                    FCasePattern ->
                        def ()

                    FCaseResult ->
                        update_last FCasePattern loc

                    FRecordFieldValue ->
                        update_last FRecordFieldName loc

                    FTup n ->
                        if n > 0 then
                            update_last (FTup (n - 1)) loc
                        else
                            remove_last loc |> ascend_left bs

                    FBindingTyp ->
                        update_last FBindingName loc

                    FTArrowResult ->
                        update_last (FTArrowArg) loc

                    FTConsArg n ->
                        if n > 0 then
                            update_last (FTConsArg (n - 1)) loc
                        else
                            update_last FTConsName loc

                    _ ->
                        def ()

            Nothing ->
                loc


ascend_right : Bindings -> Locus -> Locus
ascend_right bs loc =
    let
        def _ =
            ascend_right bs (remove_last loc)
    in
        case get_last loc of
            Just focus ->
                case focus of
                    FApplyFun ->
                        case walk_bindings bs (remove_last loc) of
                            Just (SExp (Apply Horizontal fun exp)) ->
                                update_last FApplyArg loc

                            _ ->
                                def ()

                    FApplyArg ->
                        case walk_bindings bs (remove_last loc) of
                            Just (SExp (Apply HorizontalReverse fun exp)) ->
                                update_last FApplyFun loc

                            _ ->
                                def ()

                    FCasePattern ->
                        update_last FCaseResult loc

                    FRecordFieldName ->
                        update_last FRecordFieldValue loc

                    FTup n ->
                        case walk_bindings bs (remove_last loc) of
                            Just (SExp (Tup exps)) ->
                                if (n + 1) < List.length exps then
                                    update_last (FTup (n + 1)) loc
                                else
                                    def ()

                            _ ->
                                def ()

                    FBindingName ->
                        update_last FBindingTyp loc

                    FTConsName ->
                        case walk_bindings bs (remove_last loc) of
                            Just (STyp (TCons qname typs)) ->
                                if length typs > 0 then
                                    update_last (FTConsArg 0) loc
                                else
                                    def ()

                            _ ->
                                def ()

                    FTConsArg n ->
                        case walk_bindings bs (remove_last loc) of
                            Just (STyp (TCons qname typs)) ->
                                if length typs > (n + 1) then
                                    update_last (FTConsArg (n + 1)) loc
                                else
                                    def ()

                            _ ->
                                def ()

                    FTArrowArg ->
                        update_last (FTArrowResult) loc

                    _ ->
                        def ()

            Nothing ->
                loc


ascend_up : Bindings -> Locus -> Locus
ascend_up bs loc =
    let
        def _ =
            ascend_up bs (remove_last loc)
    in
        case get_last loc of
            Just focus ->
                case focus of
                    FApplyFun ->
                        case walk_bindings bs (remove_last loc) of
                            Just (SExp (Apply VerticalReverse fun exp)) ->
                                update_last FApplyArg loc

                            _ ->
                                def ()

                    FApplyArg ->
                        case walk_bindings bs (remove_last loc) of
                            Just (SExp (Apply Vertical fun exp)) ->
                                update_last FApplyFun loc

                            _ ->
                                def ()

                    FCase n ->
                        if n > 0 then
                            update_last (FCase (n - 1)) loc
                        else
                            def ()

                    FBinding n ->
                        case walk_bindings bs (remove_last loc) of
                            Just (SBindings bindings) ->
                                case bindings |> DictList.indexOfKey n of
                                    Just i ->
                                        if i > 0 then
                                            case bindings |> DictList.getKeyAt (i - 1) of
                                                Just new ->
                                                    remove_point (update_last (FBinding new) loc) ++ [ FBindingValue, FPoint ]

                                                Nothing ->
                                                    def ()
                                        else
                                            def ()

                                    Nothing ->
                                        def ()

                            _ ->
                                def ()

                    FBindingValue ->
                        update_last FBindingName loc

                    _ ->
                        def ()

            _ ->
                loc


ascend_down : Bindings -> Locus -> Locus
ascend_down bs loc =
    let
        def _ =
            ascend_down bs (remove_last loc)
    in
        case get_last loc of
            Just focus ->
                case focus of
                    FApplyFun ->
                        case walk_bindings bs (remove_last loc) of
                            Just (SExp (Apply Vertical fun exp)) ->
                                update_last FApplyArg loc

                            _ ->
                                def ()

                    FApplyArg ->
                        case walk_bindings bs (remove_last loc) of
                            Just (SExp (Apply VerticalReverse fun exp)) ->
                                update_last FApplyFun loc

                            _ ->
                                def ()

                    FCase n ->
                        case walk_bindings bs (remove_last loc) of
                            Just (SExp (Case cases)) ->
                                if (n + 1) < List.length cases then
                                    update_last (FCase (n + 1)) loc
                                else
                                    def ()

                            _ ->
                                def ()

                    FBinding n ->
                        case walk_bindings bs (remove_last loc) of
                            Just (SBindings bindings) ->
                                case bindings |> DictList.indexOfKey n of
                                    Just i ->
                                        case bindings |> DictList.getKeyAt (i + 1) of
                                            Just new ->
                                                remove_point (update_last (FBinding new) loc) ++ [ FBindingName, FPoint ]

                                            Nothing ->
                                                def ()

                                    Nothing ->
                                        def ()

                            _ ->
                                def ()

                    FBindingName ->
                        update_last FBindingValue loc

                    FBindingTyp ->
                        update_last FBindingValue loc

                    _ ->
                        def ()

            _ ->
                loc


movement : Key -> Model -> Model
movement key model =
    let
        ( ascend, descend, vertical ) =
            case key of
                ArrowLeft ->
                    ( ascend_left, descend_right, False )

                ArrowRight ->
                    ( ascend_right, descend_left, False )

                ArrowUp ->
                    ( ascend_up, descend_left, True )

                ArrowDown ->
                    ( ascend_down, descend_left, True )

                _ ->
                    ( always identity, always identity, False )

        ascended_cursor =
            ascend model.modul.bindings model.cursor

        tail =
            List.drop (List.length ascended_cursor - 1) model.cursor

        ascended_walked =
            walk_bindings model.modul.bindings ascended_cursor

        tailed =
            remove_point ascended_cursor ++ tail

        walked =
            walk_bindings model.modul.bindings tailed

        cursor =
            case ascended_walked of
                Just syn ->
                    case ( vertical, List.length tail > 1, walked ) of
                        ( True, True, Just syn2 ) ->
                            remove_point tailed
                                ++ (descend syn2 [])
                                ++ [ FPoint ]

                        _ ->
                            (remove_point ascended_cursor) ++ (descend syn []) ++ [ FPoint ]

                _ ->
                    ascended_cursor
    in
        { model
            | cursor =
                if cursor == [ FPoint ] then
                    model.cursor
                else
                    cursor
        }


select_parent : Locus -> Bindings -> Locus
select_parent locus bindings =
    if length locus > 3 then
        case get_last locus of
            Just FTConsName ->
                case walk_bindings bindings (remove_last locus) of
                    Just (STyp (TCons _ [])) ->
                        remove_last (remove_last locus)

                    _ ->
                        remove_last locus

            _ ->
                case get_last (remove_last locus) of
                    Just (FCase 0) ->
                        case walk_bindings bindings (remove_last (remove_last locus)) of
                            Just (SExp (Case cases)) ->
                                if List.length cases == 1 then
                                    remove_last (remove_last locus)
                                else
                                    remove_last (locus)

                            _ ->
                                remove_last locus

                    --Just (FTCons 0) ->
                    _ ->
                        remove_last locus
    else
        locus
