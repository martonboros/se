module LLang exposing (..)

import Lang


type Shity t l g
    = Terminaly t
    | Listy (List l)
    | Gridy (List (List g))


type Orientation
    = Horizontal
    | Vertical


type Exp
    = Terminal TerminalExp
    | Container Orientation ContainerExp


type ContainerExp
    = Apply Exp (List Exp)
    | Infix Exp (List Exp)
    | Tup (List Exp)
    | Case (List ( Exp, Exp ))
    | Record (List ( String, Exp ))


type TerminalExp
    = Var String
    | Lit Lang.Literal


type Syntax
    = SExp Exp
    | SName String


ct_start : ContainerExp -> Syntax
ct_start cx =
    SName "e"
