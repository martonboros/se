{ pkgs ? import <nixpkgs> {}, ...} :
pkgs.stdenv.mkDerivation rec {
  name = "gore";
  buildInputs = [ pkgs.git pkgs.nodejs pkgs.elmPackages.elm ];
  shellHook = ''
    set +e
    npm install
    npm uninstall elm
    set -e
  '';
}
